﻿$ErrorActionPreference = 'Stop';

$packageName= 'HeidiSQL'
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = 'https://www.heidisql.com/installers/HeidiSQL_12.10.0.7000_Setup.exe'

$packageArgs = @{
  packageName   = $packageName
  unzipLocation = $toolsDir
  fileType      = 'exe'
  url           = $url

  silentArgs   = '/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP-' # Inno Setup
  validExitCodes= @(0)

  # optional, highly recommended
  softwareName  = 'HeidiSQL*' #part or all of the Display Name as you see it in Programs and Features. It should be enough to be unique
  checksum      = 'd50d07273ab0393bbb373c8da84b00ca96783174'
  checksumType  = 'sha1' #default is md5, can also be sha1
  checksum64    = ''
  checksumType64= '' #default is checksumType
}

Install-ChocolateyPackage @packageArgs
